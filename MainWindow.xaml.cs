﻿using System;
using System.Windows;

namespace homework_17_6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CloseProgram(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}