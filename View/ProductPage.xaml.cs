﻿
using System;
using System.Data;
using System.Data.OleDb;
using System.Windows;
using System.Windows.Controls;
using homework_17_6.Repository;

namespace homework_17_6.View
{
    public partial class ProductPage
    {
        private readonly OleDbDataAdapter _oleDbDataAdapter = ProductRepository.Instance().DataAdapter();
        private readonly DataTable _dataTable = new();
        private DataRowView _dataRowView;
        public ProductPage()
        {
            InitializeComponent();
            
            _oleDbDataAdapter.Fill(_dataTable);

            ProductsDataGrid.DataContext = _dataTable.DefaultView;
        }
        
        private void ProductsDataGridCurrentCellChanged(object sender, EventArgs e)
        {
            _dataRowView = (DataRowView) ProductsDataGrid.SelectedItem;
            _dataRowView.BeginEdit();
        }

        private void ProductsDataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (_dataRowView == null)
                return;

            _dataRowView.EndEdit();
            _oleDbDataAdapter.Update(_dataTable);
        }
        
        private void MenuItemAddProductClick(object sender, RoutedEventArgs e)
        {
            var dataRow = _dataTable.NewRow();
            var addProductWindow = new AddProductWindow(dataRow);
            
            addProductWindow.ShowDialog();


            if (addProductWindow.DialogResult == null || !addProductWindow.DialogResult.Value) return;
            
            _dataTable.Rows.Add(dataRow);
            _oleDbDataAdapter.Update(_dataTable);
        }
        
        private void MenuItemDeleteProductClick(object sender, RoutedEventArgs e)
        {
            _dataRowView = (DataRowView)ProductsDataGrid.SelectedItem;
            _dataRowView.Row.Delete();
            _oleDbDataAdapter.Update(_dataTable);
        }
    }
}