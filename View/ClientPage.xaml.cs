﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using homework_17_6.Repository;

namespace homework_17_6.View
{
    public partial class ClientPage
    {
        private readonly SqlDataAdapter _sqlDataAdapter = ClientRepository.Instance().DataAdapter();
        private readonly DataTable _dataTable = new();
        private DataRowView _dataRowView;

        public ClientPage()
        {
            InitializeComponent();

            _sqlDataAdapter.Fill(_dataTable);

            ClientsDataGrid.DataContext = _dataTable.DefaultView;
        }
        
        private void ClientsDataGridCurrentCellChanged(object sender, EventArgs e)
        {
            _dataRowView = (DataRowView) ClientsDataGrid.SelectedItem;
            _dataRowView.BeginEdit();
        }

        private void ClientsDataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (_dataRowView == null)
                return;

            _dataRowView.EndEdit();
            _sqlDataAdapter.Update(_dataTable);
        }
        
        private void MenuItemAddClientClick(object sender, RoutedEventArgs e)
        {
            var dataRow = _dataTable.NewRow();
            var addClientWindow = new AddClientWindow(dataRow);
            
            addClientWindow.ShowDialog();


            if (addClientWindow.DialogResult == null || !addClientWindow.DialogResult.Value) return;
            
            _dataTable.Rows.Add(dataRow);
            _sqlDataAdapter.Update(_dataTable);
        }
        
        private void MenuItemDeleteClientClick(object sender, RoutedEventArgs e)
        {
            _dataRowView = (DataRowView)ClientsDataGrid.SelectedItem;
            _dataRowView.Row.Delete();
            _sqlDataAdapter.Update(_dataTable);
        }
    }
}