﻿using System.Data;
using System.Data.OleDb;
using homework_17_6.Repository.Database;
using static homework_17_6.Repository.Constants.MsAccessExpressionList;

namespace homework_17_6.Repository
{
    public class ProductRepository
    {
        private static ProductRepository _instance;
        private readonly MsAccessProductDatabase _msAccessProductDatabase;
        private OleDbDataAdapter _dataAdapter;

        private ProductRepository()
        {
            _msAccessProductDatabase = MsAccessProductDatabase.Instance();
            InitOleDbDataAdapter();
        }

        public static ProductRepository Instance()
        {
            return _instance ??= new ProductRepository();
        }

        public OleDbDataAdapter DataAdapter()
        {
            return _dataAdapter;
        }

        private void InitOleDbDataAdapter()
        {
            
            _dataAdapter = new OleDbDataAdapter();
            _dataAdapter.SelectCommand = SelectCommand();
            _dataAdapter.InsertCommand = InsertCommand();
            _dataAdapter.UpdateCommand = UpdateCommand();
            _dataAdapter.DeleteCommand = DeleteCommand();
        }

        private OleDbCommand SelectCommand()
        {
            return new OleDbCommand(FindAllProductExpression, _msAccessProductDatabase.Connection());
        }

        private OleDbCommand InsertCommand()
        {
            var oleDbCommand = new OleDbCommand(CreateProductExpression, _msAccessProductDatabase.Connection());
            oleDbCommand.Parameters.Add("@Email", OleDbType.VarChar, 30, "email");
            oleDbCommand.Parameters.Add("@Code", OleDbType.VarChar, 30, "code");
            oleDbCommand.Parameters.Add("@Description", OleDbType.VarChar, 30, "description");

            return oleDbCommand;
        }

        private OleDbCommand UpdateCommand()
        {
            var oleDbCommand = new OleDbCommand(UpdateProductExpression, _msAccessProductDatabase.Connection());
            oleDbCommand.Parameters.Add("@Email", OleDbType.VarChar, 30, "email");
            oleDbCommand.Parameters.Add("@Code", OleDbType.VarChar, 30, "code");
            oleDbCommand.Parameters.Add("@Description", OleDbType.VarChar, 30, "description");
            oleDbCommand.Parameters.Add("@Id", OleDbType.Integer, 0, "id");

            return oleDbCommand;
        }

        private OleDbCommand DeleteCommand()
        {
            var oleDbCommand = new OleDbCommand(DeleteByIdProductExpression, _msAccessProductDatabase.Connection());
            oleDbCommand.Parameters.Add("@Id", OleDbType.Integer, 0, "id");

            return oleDbCommand;
        }
    }
}