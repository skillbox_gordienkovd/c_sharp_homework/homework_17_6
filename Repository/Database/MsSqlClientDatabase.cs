﻿using System.Data.SqlClient;
using static homework_17_6.Repository.Constants.SqlExpressionList;

namespace homework_17_6.Repository.Database
{
    public class MsSqlClientDatabase
    {
        private const string DbName = "ClientDB";
        private const string DbUrl = "local";
        private const string DbUser = "SA";
        private const string Password = "<YourNewStrong@Passw0rd>";

        private readonly SqlConnection _connection;

        private static MsSqlClientDatabase _instance;

        private MsSqlClientDatabase()
        {
            CreateClientDbIfNotExists();

            _connection = CreateSqlConnection(DbUrl, DbName, DbUser, Password);
        }

        public static MsSqlClientDatabase Instance()
        {
            return _instance ??= new MsSqlClientDatabase();
        }

        public SqlConnection Connection()
        {
            return _connection;
        }

        private static SqlConnection CreateSqlConnection(string dbUrl, string dbName, string dbUser, string dbPassword)
        {
            return new SqlConnection(string.Format(DbConnectionString, dbUrl, dbName, dbUser, dbPassword));
        }

        private static void CreateClientDbIfNotExists()
        {
            var masterConnection = CreateSqlConnection(DbUrl, MasterDbName, DbUser, Password);

            masterConnection.Open();

            var sqlExpression = string.Format(CreateDbIfNotExistsExpression, DbName, DbName);
            var sqlCommand = new SqlCommand(sqlExpression, masterConnection);

            sqlCommand.ExecuteNonQuery();

            masterConnection.Close();
        }
    }
}