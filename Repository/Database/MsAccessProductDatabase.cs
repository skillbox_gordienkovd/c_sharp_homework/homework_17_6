﻿using System.Data.OleDb;

namespace homework_17_6.Repository.Database
{
    public class MsAccessProductDatabase
    {
        private const string DbPath = "D:/product.accdb";
        
        private readonly OleDbConnection _connection;

        private static MsAccessProductDatabase _instance;
        
        private MsAccessProductDatabase()
        {
            _connection = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={DbPath}");
        }
        
        public static MsAccessProductDatabase Instance()
        {
            return _instance ??= new MsAccessProductDatabase();
        }
        
        public OleDbConnection Connection()
        {
            return _connection;
        }
    }
}