﻿namespace homework_17_6.Repository.Constants
{
    public static class MsAccessExpressionList
    {
        public const string CreateProductExpression =
            @"INSERT INTO product(email, code, description)
            VALUES (@Email, @Code, @Description);";

        public const string UpdateProductExpression =
            @"UPDATE product
              SET email = @Email,
                  code  = @Code,
                  description = @Description
              WHERE id = @Id";

        public const string FindByEmailProductExpression = "SELECT * FROM product WHERE email = @Email";

        public const string FindAllProductExpression = "SELECT * FROM product";

        public const string DeleteByIdProductExpression = "DELETE FROM product WHERE id = @Id";
    }
}