﻿namespace homework_17_6.Repository.Constants
{
    public static class SqlExpressionList
    {
        public const string MasterDbName = "master";

        public const string DbConnectionString = @"Data Source=({0});
                                                  Initial Catalog={1};
                                                  Integrated Security=false;
                                                  User ID={2};
                                                  Password={3}";

        public const string CreateDbIfNotExistsExpression = @"IF (SELECT DB_ID('{0}')) IS NULL
                                                                CREATE DATABASE {1}";

        public const string CreateClientTableIfNotExistsExpression =
            @"IF OBJECT_ID('dbo.client') IS NULL
                CREATE TABLE dbo.client(
                    id BIGINT IDENTITY(1,1) NOT NULL PRIMARY KEY,
                    firstname NVARCHAR(30) NOT NULL,
                    patronymic NVARCHAR(30) NOT NULL,
                    lastname NVARCHAR(30) NOT NULL,
                    phone_number NVARCHAR(11),
                    email NVARCHAR(50) NOT NULL
                )";

        public const string CreateClientExpression =
            @"INSERT INTO ClientDB.dbo.client(firstname, patronymic, lastname, phone_number, email)
            VALUES (@Firstname, @Patronymic, @Lastname, @PhoneNumber, @Email);
            SET @Id = @@IDENTITY;";

        public const string UpdateClientExpression =
            @"UPDATE ClientDB.dbo.client
              SET firstname = @Firstname,
                  patronymic  = @Patronymic,
                  lastname = @Lastname,
                  phone_number = @PhoneNumber,
                  email = @Email
              WHERE id = @Id";

        public const string FindByIdClientExpression = "SELECT * FROM ClientDB.dbo.client WHERE id = @Id";

        public const string FindAllClientExpression = "SELECT * FROM ClientDB.dbo.client";

        public const string DeleteByIdClientExpression = "DELETE FROM ClientDB.dbo.client WHERE id = @Id";
    }
}