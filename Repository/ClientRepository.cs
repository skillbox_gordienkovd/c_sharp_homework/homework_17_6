﻿using System.Data;
using System.Data.SqlClient;
using homework_17_6.Repository.Database;
using static homework_17_6.Repository.Constants.SqlExpressionList;

namespace homework_17_6.Repository
{
    public class ClientRepository
    {
        private static ClientRepository _instance;
        private readonly MsSqlClientDatabase _msSqlClientDatabase;
        private SqlDataAdapter _dataAdapter;

        private ClientRepository()
        {
            _msSqlClientDatabase = MsSqlClientDatabase.Instance();
            CreateClientTableIfNotExists();
            InitSqlAdapter();
        }

        public static ClientRepository Instance()
        {
            return _instance ??= new ClientRepository();
        }

        public SqlDataAdapter DataAdapter()
        {
            return _dataAdapter;
        }

        private void CreateClientTableIfNotExists()
        {
            _msSqlClientDatabase.Connection().Open();
            
            var sqlCommand = new SqlCommand(
                CreateClientTableIfNotExistsExpression,
                _msSqlClientDatabase.Connection()
            );

            sqlCommand.ExecuteNonQuery();
            
            _msSqlClientDatabase.Connection().Close();
        }

        private void InitSqlAdapter()
        {
            _dataAdapter = new SqlDataAdapter();
            _dataAdapter.SelectCommand = SelectCommand();
            _dataAdapter.InsertCommand = InsertCommand();
            _dataAdapter.UpdateCommand = UpdateCommand();
            _dataAdapter.DeleteCommand = DeleteCommand();
        }

        private SqlCommand SelectCommand()
        {
            return new SqlCommand(FindAllClientExpression, _msSqlClientDatabase.Connection());
        }

        private SqlCommand InsertCommand()
        {
            var sqlCommand = new SqlCommand(CreateClientExpression, _msSqlClientDatabase.Connection());
            sqlCommand.Parameters.Add("@id", SqlDbType.BigInt, 0, "id").Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add("@Firstname", SqlDbType.NVarChar, 30, "firstname");
            sqlCommand.Parameters.Add("@Patronymic", SqlDbType.NVarChar, 30, "patronymic");
            sqlCommand.Parameters.Add("@Lastname", SqlDbType.NVarChar, 30, "lastname");
            sqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar, 11, "phone_number");
            sqlCommand.Parameters.Add("@Email", SqlDbType.NVarChar, 50, "email");

            return sqlCommand;
        }

        private SqlCommand UpdateCommand()
        {
            var sqlCommand = new SqlCommand(UpdateClientExpression, _msSqlClientDatabase.Connection());
            sqlCommand.Parameters.Add("@Firstname", SqlDbType.NVarChar, 30, "firstname");
            sqlCommand.Parameters.Add("@Patronymic", SqlDbType.NVarChar, 30, "patronymic");
            sqlCommand.Parameters.Add("@Lastname", SqlDbType.NVarChar, 30, "lastname");
            sqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar, 11, "phone_number");
            sqlCommand.Parameters.Add("@Email", SqlDbType.NVarChar, 50, "email");
            sqlCommand.Parameters.Add("@Id", SqlDbType.BigInt, 0, "id");

            return sqlCommand;
        }

        private SqlCommand DeleteCommand()
        {
            var sqlCommand = new SqlCommand(DeleteByIdClientExpression, _msSqlClientDatabase.Connection());
            sqlCommand.Parameters.Add("@Id", SqlDbType.BigInt, 0, "id");

            return sqlCommand;
        }
    }
}