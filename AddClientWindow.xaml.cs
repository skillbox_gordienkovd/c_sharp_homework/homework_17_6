﻿using System.Data;

namespace homework_17_6
{
    public partial class AddClientWindow
    {
        private AddClientWindow()
        {
            InitializeComponent();
        }

        public AddClientWindow(DataRow dataRow) : this()
        {
            Cancel.Click += delegate
            {
                DialogResult = false;
            };

            Add.Click += delegate
            {
                dataRow["firstname"] = Firstname.Text;
                dataRow["patronymic"] = Patronymic.Text;
                dataRow["lastname"] = Lastname.Text;
                dataRow["phone_number"] = PhoneNumber.Text;
                dataRow["email"] = Email.Text;

                DialogResult = true;
            };
        }
    }
}