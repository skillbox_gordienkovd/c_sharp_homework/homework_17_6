﻿using System.Data;

namespace homework_17_6
{
    public partial class AddProductWindow
    {
        private AddProductWindow()
        {
            InitializeComponent();
        }

        public AddProductWindow(DataRow dataRow) : this()
        {
            Cancel.Click += delegate
            {
                DialogResult = false;
            };

            Add.Click += delegate
            {
                dataRow["email"] = Email.Text;
                dataRow["code"] = Code.Text;
                dataRow["description"] = Description.Text;

                DialogResult = true;
            };
        }
    }
}